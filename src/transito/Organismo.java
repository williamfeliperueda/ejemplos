/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transito;

import Mipaquerte.AgenteJpaController;
import Mipaquerte.MotivomultaJpaController;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author user
 */
public class Organismo implements Serializable {

    private List<Motivomulta> motivos = new LinkedList<>();
    private List<Licencia> licencias = new LinkedList<>();
    private List<Agente> agentes = new LinkedList<>();
    private List<Vehiculo> vehiculos = new LinkedList<>();
    private MotivomultaJpaController motivomultajpa;
    private AgenteJpaController agentejpa;

    public Organismo() {

        EntityManagerFactory emf
                = Persistence.createEntityManagerFactory("TrancitoPU");
        motivomultajpa = new MotivomultaJpaController(emf);
        agentejpa= new AgenteJpaController((emf));

    }

    public List<Motivomulta> getMotivos() {
        //    return motivos;
        return motivomultajpa.findMotivomultaEntities();
    }

    public void setMotivos(List<Motivomulta> motivos) {
        this.motivos = motivos;
    }

    public List<Licencia> getLicencias() {
        return licencias;
    }

    public void setLicencias(List<Licencia> licencias) {
        this.licencias = licencias;
    }

    public List<Agente> getAgentes() {
        //return agentes;
        return agentejpa.findAgenteEntities();
    }

    public void setAgentes(List<Agente> agentes) {
        this.agentes = agentes;
    }

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public MotivomultaJpaController getMotivomultajpa() {
        return motivomultajpa;
    }

    public void setMotivomultajpa(MotivomultaJpaController motivomultajpa) {
        this.motivomultajpa = motivomultajpa;
    }

    public void registrarlicencia(Licencia l) throws Exception {

        this.licencias.add(l);
    }

    public void registrarvehiculo(Vehiculo v) throws Exception {
        for (int i = 0; i < vehiculos.size(); i++) {

            if (v.getPlaca().equals(vehiculos.get(i).getPlaca())) {
                throw new Exception("Ya existe un vehiculo con esa placa");
            }

        }

        this.vehiculos.add(v);
    }

    public void registraragente(Agente A) throws Exception {
      //  for (int i = 0; i < agentes.size(); i++) {
          //  if (A.getNumeroplaca() == agentes.get(i).getNumeroplaca()) {
           //     throw new Exception("se esta intentando registrar dos agentes con la misma placa");

          //  }
       // }
       // this.agentes.add(A);
   agentejpa.create(A);
    
    }

    public void registarmotivo(Motivomulta m1) throws Exception {
        //  motivos.add(m1);
        motivomultajpa.create(m1);
    }

    public Agente buscaragente(int placa) throws Exception {

      //  for (int i = 0; i < agentes.size(); i++) {
          //  if (placa == agentes.get(i).getNumeroplaca()) {
          //      return agentes.get(i);
          //  } else {
         //       throw new Exception("el agente con la placa que se solicito no existe");
        //    }

      //  }
       // return null;
       return agentejpa.buscarporplaca(placa);
    }

    public Licencia buscarlicencia(long identificacion) throws Exception {

        for (int i = 0; i < licencias.size(); i++) {
            if (identificacion == licencias.get(i).getPersona().getIdentificacion()) {
                return licencias.get(i);
            } else {
                throw new Exception("no existe la licencia que se solicito ");
            }
        }
        return null;
    }

    public Vehiculo buscarvehiculo(String placa) throws Exception {
        for (int i = 0; i < vehiculos.size(); i++) {
            if (placa.equals(vehiculos.get(i).getPlaca())) {
                return vehiculos.get(i);

            } else {
                throw new Exception("Vehiculo no encontrado");
            }
        }
        return null;

    }

    public Motivomulta buscarmotivo(short codigo) throws Exception {
        // for (int i = 0; i < motivos.size(); i++) {
        // if (codigo == motivos.get(i).getCodigo()) {
        //     System.out.println("el motivo de la multa es" + motivos.get(i).getDescipcion() + "y su codigo es"
        //              + motivos.get(i).getCodigo());
        //  } else {
        //      throw new Exception("no se encontro el motivo de la multa");
        //    }
        // }

        return motivomultajpa.findMotivomulta(codigo);
    }

    public LinkedList<Licencia> Buscarlicencias(long identificacion) {
        LinkedList<Licencia> encontradas = new LinkedList<>();
        for (Licencia licencia : this.licencias) {
            if (licencia.getPersona().getIdentificacion() == identificacion) {
                encontradas.add(licencia);
            }
        }
        return encontradas;

    }

}
