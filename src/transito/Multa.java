/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*WILLIAM FELIPE RUEDA BEJARANO CODIGO:1763662-2711*/
package transito;

import java.io.Serializable;
import java.util.List;
import java.util.LinkedList;
import java.util.Date;

/**
 *
 * @author user
 */
public class Multa implements Serializable{

    private int valor;
    private Vehiculo vehiculo;
    private Date fecha;
    private Persona persona;
    private List<Motivomulta> motivo = new LinkedList<>();

    public Multa(int valor, Vehiculo vehiculo, Date fecha, Persona persona) throws Exception {
        if (valor < 0) {
            throw new Exception("el valor de la multa debe ser mayor a 0");
        }
        this.valor = valor;
        this.vehiculo = vehiculo;
        this.fecha = fecha;
        this.persona = persona;
    }

    public int getValor() {
        return valor;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Date getFecha() {
        return fecha;
    }

    public Persona getPersona() {
        return persona;
    }

    public List<Motivomulta> getMotivo() {
        return motivo;
    }

}
