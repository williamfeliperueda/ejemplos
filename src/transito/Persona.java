/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transito;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author user
 */
@Entity
public class Persona implements Serializable{
    @Id
    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona() {
    }

    public Persona(long identificacion, String nombre, String apellido) throws Exception {
        if (identificacion < 0) {
            throw new Exception("el valor de la dientificacion debes ser positivo ");
        }
        this.identificacion = identificacion;

        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getApellido() {
        return apellido;
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    

}
