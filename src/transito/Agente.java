
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transito;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author user
 */
@Entity
@NamedQueries (@NamedQuery(name="Buscar_agente_por_placa",
        query="SELECT a FROM Agente a WHERE a.numeroplaca = :numeroplaca"))

public class Agente extends Persona {
    @Column(nullable = false, unique = true) 
    private int numeroplaca;

    public Agente() {
    }

    public Agente(int numeroplaca, long identificacion, String nombre, String apellido) throws Exception {
        super(identificacion, nombre, apellido);
        if (numeroplaca < 0) {
            throw new Exception("el numero de la placa debe ser mayor a 0");
        }
        this.numeroplaca = numeroplaca;
    }

    public int getNumeroplaca() {
        return numeroplaca;
    }

    public void setNumeroplaca(int numeroplaca) {
        this.numeroplaca = numeroplaca;
    }
    

}
