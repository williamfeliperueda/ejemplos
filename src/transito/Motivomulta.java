/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transito;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author user
 */
@Entity 
public class Motivomulta implements Serializable {
   
    @Id
    private short codigo;
    private String descipcion;
    private int valor;

    public Motivomulta() {
    }

    public Motivomulta(short codigo, String descipcion, int valor) throws Exception {
        if (codigo < 0) {
            throw new Exception("el  codigo no puede ser menor a 0, digitar solo caracteres numericos");
        }
        this.codigo = codigo;
        this.descipcion = descipcion;
        if (valor <= 0) {
            throw new Exception("El valor de la multa no puede ser menor ni igual  a 0");
        }
        this.valor = valor;
    }

    public short getCodigo() {
        return codigo;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public int getValor() {
        return valor;
    }

    public void setCodigo(short codigo) {
        this.codigo = codigo;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

}
