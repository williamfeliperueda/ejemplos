/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transito;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author user
 */
public class Licencia implements Serializable {

    private Persona persona;
    private Date fechadeexpedicion;
    private Categoria categoria;
    private Date fechadevencimiento;

    public Licencia(Persona persona, Date fechadeexpedicion, Categoria categoria, Date fechadevencimiento) throws Exception {
        this.persona = persona;
        
        this.fechadeexpedicion = fechadeexpedicion;
        this.categoria = categoria;
        this.fechadevencimiento = fechadevencimiento;
    }

    public Date getFechadeexpedicion() {
        return fechadeexpedicion;
    }

    public Date getFechadevencimiento() {
        return fechadevencimiento;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setFechadeexpedicion(Date fechadeexpedicion) {
        this.fechadeexpedicion = fechadeexpedicion;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void setFechadevencimiento(Date fechadevencimiento) {
        this.fechadevencimiento = fechadevencimiento;
    }

    public Persona getPersona() {
        return persona;
    }

    public Categoria getCategoria() {
        return categoria;
    }

}
