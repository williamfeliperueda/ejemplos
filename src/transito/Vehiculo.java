/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transito;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Vehiculo  implements Serializable{

    private String placa;
    private int modelo;
    private String color;
    private String marca;

    public Vehiculo(String placa, int modelo, String color, String marca) throws Exception {
        if (placa.length() > 6) {
            throw new Exception("la placa no puede contener mas de 6 digitos");
        }
       
        if (marca == null || "".equals(marca.trim())) {
            throw new Exception("El  vehiculo debe poseer una marca");
        }
        if (color == null || "".equals(marca.trim())) {
            throw new Exception("El  vehiculo debe poseer un color");
        }
        if (modelo < 1800) {
            throw new Exception("El  vehiculo debe poseer un modelo mayor");
        }
        this.placa = placa;
        this.modelo = modelo;
        this.color = color;
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public int getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

}
