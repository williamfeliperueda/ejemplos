/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mipaquerte;

import Mipaquerte.exceptions.NonexistentEntityException;
import Mipaquerte.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import transito.Motivomulta;

/**
 *
 * @author salasistemas
 */
public class MotivomultaJpaController implements Serializable {

    public MotivomultaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Motivomulta motivomulta) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(motivomulta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMotivomulta(motivomulta.getCodigo()) != null) {
                throw new PreexistingEntityException("Motivomulta " + motivomulta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Motivomulta motivomulta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            motivomulta = em.merge(motivomulta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                short id = motivomulta.getCodigo();
                if (findMotivomulta(id) == null) {
                    throw new NonexistentEntityException("The motivomulta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(short id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Motivomulta motivomulta;
            try {
                motivomulta = em.getReference(Motivomulta.class, id);
                motivomulta.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The motivomulta with id " + id + " no longer exists.", enfe);
            }
            em.remove(motivomulta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Motivomulta> findMotivomultaEntities() {
        return findMotivomultaEntities(true, -1, -1);
    }

    public List<Motivomulta> findMotivomultaEntities(int maxResults, int firstResult) {
        return findMotivomultaEntities(false, maxResults, firstResult);
    }

    private List<Motivomulta> findMotivomultaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Motivomulta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Motivomulta findMotivomulta(short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Motivomulta.class, id);
        } finally {
            em.close();
        }
    }

    public int getMotivomultaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Motivomulta> rt = cq.from(Motivomulta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
